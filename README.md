# Atwin

Atwin is a digitisation of Franco Grignani’s Gemini.


## Blurb

_Atwin_ is a modern remake of Franco Grignani’s _Gemini_.
The name Atwin is a pun: “A twin” = Gemini, the twin of the zodiac.
It is inspired by the angular and unusual forms of the numbers
on bank cheques (so-called MICR).

Strangely rounded rectangles make the glyphs appear unfamiliar.
It makes for an angular but also blobby design that disrupts and
breaks away from tradition.

You should use Atwin to add flair and confidence to sci-fi,
futurist, outré, or just plain unusual materials.
Good in displays sizes.

Latin-based scripts are well supported with a generous supply of
punctuation and diacritics.

13 additional alts are supplied in the A to Z alphabet.

Kerned to perfection. Tight.


## Beta programme

This font, Atwin, is currently in its experimental beta phase.

If you have a copy of the font file,
you are participating in this experiment.
Thank you for your participation.

In this round, new alternates have been drawn and both these and
the existing alternates have been added to OpenType Layout features.
These alternates have also been furnished with accent marks.

## 2023 refresh

In 2023 a significant refresh of Atwin was done, primarily
working on new alternates and making OpenType Stylistic Set
features for them.
A few new glyphs have been added, and a few redrawn.

- Move alts to OpenType feature.ss03s (/A.cv01 /D.cv01 /E.ss03
  /W.ss02 /AE.cv01 /exclam.ss03)
- Add accents to alt morphs
- Improve crossbar on /Eth
- Add /G.cv01 alt without crossbar
- Add /E.ss01 /S.ss01 /U.ss01 monoline
- Add /A.ss02 /V.ss02 solid serif
- Add /P.ss03 stepped on left
- Add /N.ss04 /O.ss04 rotated

- Add /hryvnia; redraw /yen /sterling
- Redraw /at /numbersign /multiply /slash /bar
- Redraw /dagger /paragraph /section
- Redraw U+0326 COMBINING COMMA BELOW
- Redraw U+0327 COMBINING CEDILLA
- Add U+2031 PER TEN THOUSAND
- Add U+2206 INCREMENT
- Add U+2338 APL FUNCTIONAL SYMBOL QUAD EQUAL
- Add U+2726 BLACK FOUR POINTED STAR

- Add the (misnamed) MICR symbols: U+2446, U+2447, U+2448, U+2449

- Improve spacing and kerning on punctuation (many changes)

## Stylistic Sets and so on

Most of the alts are organised into Stylistic Sets `ss01`
through `ss04`, none of which form a complete alphabet in themselves.
A few ad hoc forms are added as character variants in `cv01`:

- `ss01`: Monoline, a constant stroke-width form
- `ss02`: Solid Serif, /A /V /W have a single solid bar instead
  of separate serifs on each stroke
- `ss03`: Stepped on Left, a few glyphs have a morph with a
  stepped profile on the left, rather than a straight vertical
- `ss04`: Flipped and Turned, a couple of glyphs have flipped
  morphs

- `cv01`: /A /D /G have morphs which are not otherwise related
  to others and so don’t belong in a set

The sets are somewhat aspirational in that in each case there
exists the future possibility of drawing more morphs and adding
them to the stylistic sets.


## MICR and spacing

The numbers are from MICR.
They are an approximate copy of E13-B found on cheques.

From a grey copy of that standard we see that the standardised
spacing is 3.175mm ±0.254mm.
That's on a cap-height of 2.972mm.

In the current design, which has a cap-height of 2800, that
makes the standardised spacing of the MICR numbers and symbols
3037±243.
Which is huge for the current very tight spacing, but merely
generous considered on its own merits.


## Fit and Spacing

Recall that the UPM is 4000.

Standard spacing is 84 O 84.

From the sample, the spacing is fairly tight, and the kerning even more so.

D G H N U same as O.

Glyphs with an incomplete vertical edge on the left- or right-hand side
have a space of 73 or occasionally 74 (this arises from the fact
that 21 × 3.5 is 73.5 exactly, and rounding has happened; i
should probably regularise this:
B C E F J K P R S Y.

Glyphs with just a serif (or stub) on the left- or right-hand side
have a space of 12 on that side:
I M Q T V W X Z

For /X this is measured from the top left serif which is
slightly to the right of the bottom left terminal which has the
xMin node.

A and L would be in that list, but they have glyph specific
adjustments.


## Experimental TTX surgery

What if you want GSUB rules that Glyphs Mini doesn't support?
Well, you're supposed to spend money on Glyphs (full).

But you can sort of do it with `makeotf`.
`makeotf` is a tool from the AFDKO that takes glyphs in a font file,
and compiles features described in text files, and creates a new OTF file.

This experiment is documented here just for the sake of documenting it
somewhere.

So we can add GSUB rules in a `features` file and `makeotf` will make a new
OTF file with them in.
But it doesn't use the features in the source font file.

One fairly fragile workaround is to `ttx` both source and compiled font
files, take the GSUB feature and lookups out of the compiled ttx, and add
them to the source ttx.
You have to adjust the Feature indexes and the Lookup indexes.

Then you can convert the `.ttx` back into a `.otf` using ttx.
And it works!

# END

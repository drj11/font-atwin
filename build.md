# The Atwin Build process

## Rapid and probably incomplete

The abstract version is
0.YYYYMMDD

from which potentially more than one release might be made
(although that would be unusual).

For Glyphs, which only allows a fairly restricted idea of a
version, that abstract version is encoded into a concrete Glyphs
version which is:

    y.j

Where _y_ is YYYY-2020 (so it is 1 for the year 2021);
and _j_ is a 3 digit (0-padded) decimal value that is
the ISO 8601 cardinal date (`date +%j` will gives this).

Make a branch in git:

    git checkout -b 0.YYYYMMDD

Open glyphs file; change info:

Remove LAB from Name on info page;

Add / update Copyright.
Change License to

    No distribution permitted, except according to your proper licence agreement.

Save that, and export a new OTF file.
Ensure Remove Overlap and Autohint are enabled.

Close in Glyphs (most reliable to use the mouse).

Add files to git and commit
